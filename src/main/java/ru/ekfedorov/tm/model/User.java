package ru.ekfedorov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.enumerated.Role;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractEntity {

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    private boolean lock = false;

    @Nullable
    private String login;

    @Nullable
    private String middleName;

    @Nullable
    private String passwordHash;

    @Nullable
    private Role role = Role.USER;

}
